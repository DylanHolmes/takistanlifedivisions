package com.killersmurf.takistanlifedivisions.listener;

import com.killersmurf.takistanlifedivisions.TakistanLifeDivisions;
import com.killersmurf.takistanlifedivisions.division.Division;
import com.killersmurf.takistanlifedivisions.player.TLDPlayer;
import com.killersmurf.takistanlifedivisions.util.PlayerUtil;
import java.util.ArrayList;
import java.util.List;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerChatEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.ItemStack;

/**
 *
 * @author holmd834
 */
public class TLDListener implements Listener {

    private TakistanLifeDivisions tld;

    /**
     *
     *
     * @param tld
     */
    public TLDListener(TakistanLifeDivisions tld) {
        this.tld = tld;
    }

    /**
     * Adds the player to the PlayerMap in a certain way based on if they have
     * played before, if they have already been added then it will return.
     *
     * @param event
     */
    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        if (!player.hasPlayedBefore()) {
            PlayerUtil.addPlayer(player.getName(), false);
            return;
        }
        PlayerUtil.addPlayer(player.getName(), true);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerMove(PlayerMoveEvent event) {
        TLDPlayer player = PlayerUtil.getPlayer(event.getPlayer());
        if (player.isRestrained() || player.isStunned()) {
            event.setTo(event.getFrom());
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerEntityInteract(PlayerInteractEntityEvent event) {
        if (!(event.getRightClicked() instanceof Player)) {
            return;
        }
        TLDPlayer player = PlayerUtil.getPlayer(event.getPlayer());
        ItemStack item = player.getPlayer().getItemInHand();
        TLDPlayer clicked = PlayerUtil.getPlayer((Player) event.getRightClicked());
        Division div = player.getDivision();
        Division clickedDiv = clicked.getDivision();
        
        if (div.equals(Division.POLICE) && clickedDiv.equals(Division.CIVILIAN) && clicked.isRestrained()) {
            if (item.getType().equals(Material.STRING)) {
                player.sendMessage("" + ChatColor.BOLD + ChatColor.AQUA + 
                        "Do you wish to arrest this player? type /arrest <seconds> <reason>");
                player.sendMessage("" + ChatColor.BOLD + ChatColor.AQUA + "Or left click the player with the string to release him.");
            }
        }
    }

    /**
     *
     * @param event
     */
    @EventHandler(priority = EventPriority.HIGHEST)
    public void onItemInteract(PlayerInteractEvent event) {
        TLDPlayer player = PlayerUtil.getPlayer(event.getPlayer());
        if (!event.getAction().equals(Action.LEFT_CLICK_AIR)) {
            return;
        }
        ItemStack item = event.getItem();
        if (item.getType() == null || !PlayerUtil.isWeapon(item.getType()) 
                && !PlayerUtil.isUtilItem(item.getType())) {
            event.setCancelled(true);
        }
    }

    /**
     *
     * An event that is triggered when two players attack each other. Based on
     * who attacked first someone gets karma. If a cop attacked a Civilian with
     * a stunning weapon it will stun the civilian.
     *
     * @param event
     */
    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerAttack(EntityDamageEvent event) {
        if (!(event.getEntity() instanceof Player)) {
            return;
        }
        if (!(event instanceof EntityDamageByEntityEvent)) {
            return;
        }
        EntityDamageByEntityEvent e = (EntityDamageByEntityEvent) event;
        if (!(e.getDamager() instanceof Player)) {
            return;
        }
        TLDPlayer damager = PlayerUtil.getPlayer((Player) e.getDamager());
        TLDPlayer attacked = PlayerUtil.getPlayer((Player) e.getEntity());
        Division div = damager.getDivision();
        if (div.equals(Division.CIVILIAN)) {
            if (!tld.isInCombat(damager.getName(), attacked.getName())) {
                return;
            }
            damager.incrementKarma(1);
        } else if (div.equals(Division.POLICE)) {
            ItemStack weapon = damager.getPlayer().getItemInHand();
            if (weapon.getType().equals(Material.BOW)
                    && weapon.containsEnchantment(Enchantment.ARROW_KNOCKBACK)) {
                Bukkit.broadcastMessage(ChatColor.AQUA + damager.getName() + " stunned " + attacked.getName() + ".");
                attacked.setStunned(100);
            } else if (weapon.getType().equals(Material.STICK)
                    && weapon.containsEnchantment(Enchantment.KNOCKBACK)) {
                attacked.setStunned(50);
                Bukkit.broadcastMessage(ChatColor.AQUA + damager.getName() + " stunned " + attacked.getName() + ".");

            }
        }
    }

    /**
     * prevents new people from talking and formats the chat.
     *
     * @param event
     */
    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerChat(PlayerChatEvent event) {
        TLDPlayer player = PlayerUtil.getPlayer(event.getPlayer());
        event.setCancelled(true);
        if (player.getDivision().getName().equals("unknown")) {
            player.sendMessage(ChatColor.YELLOW + "You cannot chat until you've chosen a Division.");
            player.sendMessage(ChatColor.YELLOW + "To choose a Division, go to /spawn and right click a Division sign.");
            player.sendMessage(ChatColor.YELLOW + "Or type /Division join <Division Name> [Police, Insurgent, Government, Civilian]");
            return;
        }
        String message = event.getMessage();
        Division div = player.getDivision();
        Bukkit.broadcast(ChatColor.DARK_GRAY + "[" + div.getColor() + div.getName().toUpperCase()
                + ChatColor.DARK_GRAY + "]" + ChatColor.WHITE + player.getName() + ": "
                + message, "takistainlifedivisions.chat.see");
    }

    /**
     * Triggered when a Cop kills a Civ.
     *
     * @param event
     */
    @EventHandler(priority = EventPriority.HIGHEST)
    public void onCopKillCiv(PlayerDeathEvent event) {
        Player player = event.getEntity();
        if (!(player.getKiller() instanceof Player)) {
            return;
        }
        event.setDeathMessage(null);
        TLDPlayer killer = PlayerUtil.getPlayer(player.getKiller());
        TLDPlayer killed = PlayerUtil.getPlayer(player);
        String killerName = killer.getName();
        String killedName = killed.getName();
        if (killer.getDivision().equals(Division.POLICE)
                && killed.getDivision().equals(Division.CIVILIAN)) {
            if (killed.getKarma() > 5 || tld.isInCombat(killedName, killerName)) {
                String armed = (PlayerUtil.containsWeapon(killed.getPlayer().getInventory())) ? "ARMED" : "UNARMED";
                Bukkit.broadcastMessage(ChatColor.GREEN + killerName + " killed " + killedName
                        + ", an " + armed + " Civilian.");
                if (armed.equals("UNARMED")) {
                    killer.incrementKarma(1);
                }
                return;
            }
            killer.incrementKarma(2);
            Bukkit.broadcastMessage(ChatColor.GREEN + killerName + " killed " + killedName
                    + ", an innocent Civilian.");
            killer.sendMessage(ChatColor.RED + "You've killed an innocent Civilian, you should compensate them.");
            killer.sendMessage(ChatColor.RED + "If you do not compensate them you may be removed from the Police Force.");
            killer.sendMessage(ChatColor.RED + "You owe " + killedName + killed.getHeldMoney() + "$, Type /compensate <Player Name>");
            killer.sendMessage(ChatColor.RED + "If you cannot afford the amount it will just take the amount you have and the State (Server) will cover the rest.");
        }
    }
}