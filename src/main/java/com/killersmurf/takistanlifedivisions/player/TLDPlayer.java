/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.killersmurf.takistanlifedivisions.player;

import com.killersmurf.takistanlifedivisions.TakistanLifeDivisions;
import com.killersmurf.takistanlifedivisions.division.Division;
import com.killersmurf.takistanlifedivisions.tasks.EndJailTask;
import com.killersmurf.takistanlifedivisions.tasks.JailTask;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.getspout.spoutapi.Spout;
import org.getspout.spoutapi.player.SpoutPlayer;

/**
 *
 * @author Dylan
 */
public class TLDPlayer {
    
    private String name;
    private SpoutPlayer player;
    private Division div;
    private int karma;
    private boolean restrained;
    private boolean stunned;
    private JailTask jail;
    private float heldMoney;
    private TakistanLifeDivisions tld;

    /**
     * Constructs the player
     *
     * @param name
     * @param money
     * @param div
     */
    public TLDPlayer(String name, float money, Division div) {
        this.name = name;
        this.heldMoney = money;
        this.div = div;
        tld = TakistanLifeDivisions.getInstance();
        updateTitle();
    }
    
    public boolean isJailed() {
        return getJail() != null;
    }

    /**
     * Another Constructor
     *
     * @param name
     */
    public TLDPlayer(String name) {
        this.name = name;
        this.player = Spout.getServer().getPlayer(name);
        tld = TakistanLifeDivisions.getInstance();
        load();
        updateTitle();
    }

    /**
     *
     * @param player
     */
    public TLDPlayer(SpoutPlayer player) {
        this.player = player;
        this.name = player.getName();
        tld = TakistanLifeDivisions.getInstance();
        load();
        updateTitle();
    }
    
    public JailTask getJail() {
        return jail;
    }
    
    public void setJail(JailTask jail) {
        this.jail = jail;
    }
    
    public void arrest(long duration) {
        double x, y, z;
        YamlConfiguration file = (YamlConfiguration) TakistanLifeDivisions.getInstance().getConfig();
        x = file.getDouble("jail.loc.x");
        y = file.getDouble("jail.loc.y");
        z = file.getDouble("jail.loc.z");
        String world = file.getString("jail.world.name", "world");
        player.teleport(new Location(Spout.getServer().getWorld(world), x, y, z));
        player.sendMessage(ChatColor.AQUA + "You've been arrested for " + (duration / 20) + " seconds.");
        player.sendMessage(ChatColor.AQUA + "You will be released automaticaly.");
        jail = new JailTask(this, ((duration / 20) * 100));
        int id = Spout.getServer().getScheduler().scheduleSyncRepeatingTask(tld, jail, 20L, 100L);
        Spout.getServer().getScheduler().scheduleSyncDelayedTask(tld, new EndJailTask(id), duration / 20);
    }
    
    public void release() {
        double x, y, z;
        String world;
        FileConfiguration file = TakistanLifeDivisions.getInstance().getConfig();
        x = file.getDouble("civ.spawn.x");
        y = file.getDouble("civ.spawn.y");
        z = file.getDouble("civ.spawn.z");
        world = file.getString("world.main.name", "world");
        player.teleport(new Location(Spout.getServer().getWorld(world), x, y, z));
        player.sendMessage(ChatColor.AQUA + "You've been released. Try to obey. Or just don't get caught.");
    }

    /**
     * Returns true if the player is stunned and false if not.
     *
     * @return stunned
     */
    public boolean isStunned() {
        return stunned;
    }

    /**
     * Returns the amount of karma the player has.
     *
     * @return karma
     */
    public int getKarma() {
        return karma;
    }

    /**
     * Sets the amount of karma the player has.
     *
     * @param karma
     */
    public void setKarma(int karma) {
        this.karma = karma;
    }

    /**
     * Returns the Spout.getServer() player that is tied to this instance.
     *
     * @return
     */
    public SpoutPlayer getPlayer() {
        return player;
    }

    /**
     * Increments the amount of karma this player has by amt.
     *
     * @param amt
     */
    public void incrementKarma(int amt) {
        karma += amt;
    }

    /**
     * decrements the amount of karma this player has by amt.
     *
     * @param amt
     */
    public void decrementKarma(int amt) {
        karma -= amt;
    }

    /**
     * Sets the player stunned for a specified amount of time.
     *
     * @param time
     */
    public void setStunned(int time) {
        player.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, time, 2));
        player.addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, time, 2));
        player.addPotionEffect(new PotionEffect(PotionEffectType.WEAKNESS, time, 2));
        player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, time, 2));
        stunned = true;
        Spout.getServer().getScheduler().scheduleSyncDelayedTask(tld, new Runnable() {
            @Override
            public void run() {
                stunned = false;
            }
        }, time);
    }

    /**
     * Sends a message to the player.
     *
     * @param msg
     */
    public void sendMessage(String msg) {
        player.sendMessage(msg);
    }

    /**
     * Gets the name of the player.
     *
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * Gets the Division of the player.
     *
     * @return div
     */
    public Division getDivision() {
        return div;
    }

    /**
     * Sets the Division of the player.
     *
     * @param div
     */
    public void setDivision(Division div) {
        this.div = div;
    }

    /**
     * Gets the amount of money that they currently have on them.
     *
     * @return heldMoney
     */
    public float getHeldMoney() {
        return heldMoney;
    }

    /**
     * Sets the amount of money that they are currently holding.
     *
     * @param heldMoney
     */
    public void setHeldMoney(float heldMoney) {
        this.heldMoney = heldMoney;
    }

    /**
     * Saves this player.
     */
    public void save() {
        YamlConfiguration dataFile = TakistanLifeDivisions.getDataFile();
        dataFile.set("players." + name + ".division", getDivision().getName());
        dataFile.set("players." + name + ".money", getHeldMoney());
        File file = new File("./data.yml");
        try {
            dataFile.save(file);
        } catch (IOException ex) {
            Logger.getLogger(TLDPlayer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Loads this player.
     */
    public final void load() {
        YamlConfiguration dataFile = TakistanLifeDivisions.getDataFile();
        setHeldMoney((float) dataFile.getLong("players." + name + ".money"));
        setDivision(Division.valueOf(dataFile.getString("players." + name + ".division")));
    }

    /**
     * Sets the Title above their head to the colour of their division. Why
     * doesn't Netbeans recognize Colour as a word?
     */
    public final void updateTitle() {
        player.setTitle(div.getColor() + "[" + div.getShortName() + "] " + getName());
    }

    /**
     * Sets the player restrained.
     *
     * @param b
     */
    public void setRestrained(boolean b) {
        //TODO: Think of a way to make it so the player can't move with out rubberingbanding. 
    }

    /**
     * Returns true if the player is restrained and false if not.
     *
     * @return
     */
    public boolean isRestrained() {
        return restrained;
    }
}
