/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.killersmurf.takistanlifedivisions.tasks;

import com.killersmurf.takistanlifedivisions.player.TLDPlayer;
import org.bukkit.ChatColor;

/**
 *
 * @author holmd834
 */
public class JailTask implements Runnable {

    private TLDPlayer player;
    public float bail;

    public JailTask(TLDPlayer player, float bail) {
        this.player = player;
        this.bail = bail;
    }

    @Override
    public void run() {
        bail -= 500;
        player.sendMessage(ChatColor.AQUA + "Bail is now at $" + bail + ".");
        player.sendMessage(ChatColor.AQUA + "Type /bail pay <amount> to pay your bail.");
        if (bail < 500) {
            player.release();
            player.setJail(null);
        }
    }
}
