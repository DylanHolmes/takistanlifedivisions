/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.killersmurf.takistanlifedivisions.tasks;

import org.bukkit.Bukkit;

/**
 *
 * @author holmd834
 */
public class EndJailTask implements Runnable {

    private int id;

    public EndJailTask(int id) {
        this.id = id;
    }

    @Override
    public void run() {
        Bukkit.getScheduler().cancelTask(id);
    }
}
