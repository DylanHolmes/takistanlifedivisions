package com.killersmurf.takistanlifedivisions;

import com.killersmurf.takistanlifecore.util.TakistanCommand;
import com.killersmurf.takistanlifedivisions.commands.ArrestCommand;
import com.killersmurf.takistanlifedivisions.commands.BailCommand;
import com.killersmurf.takistanlifedivisions.listener.TLDListener;
import com.killersmurf.takistanlifedivisions.player.TLDPlayer;
import com.killersmurf.takistanlifedivisions.util.PlayerUtil;
import java. io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * @author Dylan Holmes
 *
 */
public class TakistanLifeDivisions extends JavaPlugin {

    /**
     * Static definitions
     */
    public static final Logger LOGGER = Logger.getLogger("Minecraft");
    private static TakistanLifeDivisions instance;
    private static YamlConfiguration dataFile;
    /**
     *Non static definitions
     */
    public Map<String, String> combatMap = new HashMap<>();
    private Map<String, TakistanCommand> commandMap = new HashMap<>();

    /**
     * Enables the plugin.
     */
    @Override
    public void onEnable() {
        //Commands
        commandMap.put("arrest", new ArrestCommand());
        commandMap.put("bail", new BailCommand());
        //Registering Commands
        TakistanCommand.registerAll(commandMap, this);
        //Loading stuffs.
        instance = this;
        loadDataFile();
        Bukkit.getPluginManager().registerEvents(new TLDListener(this), this);
        //Incase of reload
        for (Player player : Bukkit.getOnlinePlayers()) {
            String name = player.getName();
            TLDPlayer tldPlayer = new TLDPlayer(name);
            tldPlayer.load();
            PlayerUtil.addPlayer(tldPlayer);
        }
    }

    /**
     * Disables the plugin.
     */
    @Override
    public void onDisable() {
        for (TLDPlayer player : PlayerUtil.playerMap.values()) {
            player.save();
        }
        saveDataFile();
    }

    /**
     *
     * @param attacker
     * @param defender
     * @return true if the attacker attacked the defender first.
     */
    public boolean isInCombat(String attacker, String defender) {
        return combatMap.get(attacker).equalsIgnoreCase(defender);
    }

    /**
     * Puts the specified string into the combat map with the value attached to
     * it
     *
     * @param name
     * @param other
     */
    public void setInCombat(String name, String other) {
        combatMap.put(name, other);
    }

    /**
     * Removes the specified string from the combat map.
     *
     * @param name
     */
    public void setOutOfCombat(String name) {
        combatMap.remove(name);
    }

    /**
     * Gets the instance of the data file.
     *
     * @return a YamlConfiguration file.
     */
    public static YamlConfiguration getDataFile() {
        return dataFile;
    }

    /**
     * loads the data file.
     *
     * @return a YamlConfiguration file with the data from /data.yml
     */
    public YamlConfiguration loadDataFile() {
        File df = new File("./data.yml");

        if (!df.exists()) {
            try {
                df.createNewFile();
            } catch (IOException ex) {
                LOGGER.log(Level.SEVERE, "Could not create the data file!", ex);
            }
        }
        dataFile = YamlConfiguration.loadConfiguration(df);
        return dataFile;
    }

    /**
     * Saves the data file.
     */
    public static void saveDataFile() {
        File df = new File("./data.yml");
        try {
            dataFile.save(df);
        } catch (IOException ex) {
            LOGGER.log(Level.SEVERE, "Could not save the data!", ex);
        }
    }

    /**
     * Returns the instance of the plugin.
     *
     * @return instance
     */
    public static TakistanLifeDivisions getInstance() {
        return instance;
    }
}
