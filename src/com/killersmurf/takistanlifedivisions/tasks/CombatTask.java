package com.killersmurf.takistanlifedivisions.tasks;

import com.killersmurf.takistanlifedivisions.TakistanLifeDivisions;

/**
 *
 * @author holmd834
 */
public class CombatTask implements Runnable {
    private TakistanLifeDivisions tld;
    private String name;
    
    /**
     *
     * @param tld
     * @param name
     */
    public CombatTask(TakistanLifeDivisions tld, String name) {
        this.tld = tld;
        this.name = name;
    }

    @Override
    public void run() {
        tld.setOutOfCombat(name);
    }
}
