/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.killersmurf.takistanlifedivisions.commands;

import com.killersmurf.takistanlifecore.util.TakistanCommand;
import com.killersmurf.takistanlifedivisions.division.Division;
import com.killersmurf.takistanlifedivisions.player.TLDPlayer;
import com.killersmurf.takistanlifedivisions.util.PlayerUtil;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 *
 * @author holmd834
 */
public class ArrestCommand extends TakistanCommand {

    @Override
    public void execute(CommandSender sender, Command cmnd, String string, String[] args) {
        if (!(sender instanceof Player)) {
            return;
        }
        TLDPlayer player = PlayerUtil.getPlayer(sender.getName());
        if (!player.getDivision().equals(Division.POLICE)) {
            return;
        }
        if (args.length < 2) {
            return;
        }
        
        String arrested = args[0];
        TLDPlayer other = PlayerUtil.getPlayer(arrested);
        if (!other.isRestrained() || 
                !(player.getPlayer().getLocation().distance(other.getPlayer().getLocation()) > 5)) {
            sender.sendMessage(ChatColor.RED + 
                    "You must be within 5 blocks of the other player and they must be restrained.");
            return;
        }
        List<String> list = new ArrayList<>(Arrays.asList(args));
        list.remove(0);
        StringBuilder reason = new StringBuilder();

        for (int i = 0; i < list.size(); i++) {
            reason.append(list.get(i));
        }
        int time = other.getKarma() * 1200;
        other.arrest(time);
        other.sendMessage(ChatColor.DARK_AQUA + "You've been arrested for " + time/20 + " seconds for " + reason + ".");
        Bukkit.broadcastMessage(ChatColor.AQUA + other.getName() + " has been arrest by " 
                + player.getName() + " for " + time/20 + " for " + reason);
    }
}
