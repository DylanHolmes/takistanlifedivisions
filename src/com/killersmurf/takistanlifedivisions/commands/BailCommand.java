/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.killersmurf.takistanlifedivisions.commands;

import com.dylanholmes.takistanlife.economy.Bank;
import com.dylanholmes.takistanlife.economy.BankAccount;
import com.killersmurf.takistanlifecore.util.TakistanCommand;
import com.killersmurf.takistanlifedivisions.player.TLDPlayer;
import com.killersmurf.takistanlifedivisions.tasks.JailTask;
import com.killersmurf.takistanlifedivisions.util.PlayerUtil;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 *
 * @author holmd834
 */
public class BailCommand extends TakistanCommand {

    @Override
    public void execute(CommandSender sender, Command cmnd, String string, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage("DAS CONSOLE HATH NO BAIL!");
            return;
        }
        if (args.length < 1) {
            sender.sendMessage(ChatColor.RED + "You need to enter a parameter <pay | amount>. For example: /bail pay");
            return;
        }

        TLDPlayer player = PlayerUtil.getPlayer(sender.getName());
        final JailTask jail = player.getJail();
        if (jail == null) {
            player.sendMessage(ChatColor.RED + "You're not in jail.");
            return;
        }
        String operator = args[0];
        if (operator.equalsIgnoreCase("pay")) {
            BankAccount account = Bank.getAccount(player.getName());
            final float money = account.getMoney();
            if (!(money >= jail.bail)) {
                player.sendMessage(ChatColor.RED + "You do not have enough money. Please just wait it out.");
                player.sendMessage(ChatColor.RED + "Or wait until the bail is lower.");
                return;
            }
            account.subtractMoney(jail.bail);
            player.sendMessage(ChatColor.AQUA + "You've paid the bail $" + jail.bail + ".");
        } else if (operator.equalsIgnoreCase("amount")) {
            player.sendMessage(ChatColor.AQUA + "Your bail is $" + jail.bail + ".");
        } else {
            player.sendMessage(ChatColor.RED + "That is not a valid option. Or you typed something wrong.");
        }
    }
}