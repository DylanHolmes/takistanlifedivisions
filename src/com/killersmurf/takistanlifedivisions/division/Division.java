/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.killersmurf.takistanlifedivisions.division;

import org.bukkit.ChatColor;

/**
 *
 * @author Dylan
 */
public enum Division {
    
    UNKNOWN("unknown", "UNK", ChatColor.LIGHT_PURPLE),
    CIVILIAN("civilian", "CIV", ChatColor.GRAY),
    POLICE("police", "COP", ChatColor.DARK_BLUE),
    INSURGENT("insurgent", "INSG", ChatColor.RED),
    GOVERNMENT("government", "GOV", ChatColor.GREEN);
    private String name;
    private String shortName;
    private ChatColor color;
    
    private Division(String name, String shortName, ChatColor color) {
        this.name = name;
        this.shortName = shortName;
        this.color = color;
    }

    public String getName() {
        return name;
    }

    public String getShortName() {
        return shortName;
    }

    public ChatColor getColor() {
        return color;
    }
    
    

}