/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.killersmurf.takistanlifedivisions.util;

import com.killersmurf.takistanlifedivisions.division.Division;
import com.killersmurf.takistanlifedivisions.player.TLDPlayer;
import java.util.HashMap;
import java.util.Map;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.PlayerInventory;

/**
 *
 * @author holmd834
 */
public class PlayerUtil {

    /**
     * A map.
     */
    public static Map<String, TLDPlayer> playerMap = new HashMap<>();

    /**
     * A constructor
     *
     * @param name
     * @return
     */
    public static TLDPlayer getPlayer(String name) {
        return playerMap.get(name);
    }

    /**
     * A method that gets a TLDPlayer from a Bukkit Player
     *
     * @param player
     * @return
     */
    public static TLDPlayer getPlayer(Player player) {
        return getPlayer(player.getName());
    }

    /**
     * A method that adds a new TLDPlayer into the map.
     *
     * @param name
     * @param bool
     */
    public static void addPlayer(String name, boolean bool) {
        if (playerMap.containsKey(name)) {
            return;
        }
        if (bool == false) {
            playerMap.put(name, new TLDPlayer(name, 100000, Division.UNKNOWN));
            return;
        }
        TLDPlayer player = new TLDPlayer(name);
        player.load();
        playerMap.put(name, player);
    }

    /**
     * A method that adds an existing TLDPlayer into the map.
     *
     * @param player
     */
    public static void addPlayer(TLDPlayer player) {
        if (playerMap.containsKey(player.getName())) {
            return;
        }
        playerMap.put(player.getName(), player);
    }

    /**
     * Checks if the inventory contains a weapon.
     *
     * @param inventory
     * @return
     */
    public static boolean containsWeapon(PlayerInventory inventory) {

        if (inventory.contains(Material.WOOD_SWORD) || inventory.contains(Material.STONE_SWORD)
                || inventory.contains(Material.IRON_SWORD) || inventory.contains(Material.GOLD_SWORD)
                || inventory.contains(Material.DIAMOND_SWORD) || inventory.contains(Material.BOW)) {
            return true;
        }
        return false;
    }

    /**
     * Checks if the Material is a weapon
     *
     * @param type
     * @return
     */
    public static boolean isWeapon(Material type) {
        if (type.equals(Material.WOOD_SWORD) || type.equals(Material.STONE_SWORD)
                || type.equals(Material.IRON_SWORD) || type.equals(Material.GOLD_SWORD)
                || type.equals(Material.DIAMOND_SWORD) || type.equals(Material.BOW)) {
            return true;
        }
        return false;
    }

    public static boolean isUtilItem(Material type) {
        if (type.equals(Material.STRING) || type.equals(Material.BOOK_AND_QUILL)) {
            return true;
        }
        return false;
    }
}
